import React, { Component } from 'react';
import axios from 'axios'
import App from '../App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import CurrenciesList from './CurrenciesList'
import LastRates from './LastRates'
import Clock from './Clock'
import Header from './Header'
import Form from './Form'
import DropDown from './DropDown';

class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showActualRates: true,
            showLastRates: false,
        };
        this.handleClickActual = this.handleClickActual.bind(this);
        this.handleClickSearch = this.handleClickSearch.bind(this);
    }

    handleClickActual = (state) => {
        console.log(this.state)
        this.setState({
            showActualRates: true,
            showLastRates: false,
        })
    }


    handleClickSearch = (state) => {
        console.log(this.state)
        this.setState({
            showLastRates: true,
            showActualRates: false,
        })
    }

    btnClass = "btn btn-outline-success";

    render() {

        return (

            <div>
                <div className="container">
                    <div className="jumbotron">
                        <Header />
                        <div className="row">
                            <div className="btnContainer">
                                <button type="button"
                                    className={this.btnClass}
                                    onClick={this.handleClickActual}>
                                    AKTUALNE KURSY
                        </button>
                                <button type="button"
                                    className={this.btnClass}
                                    onClick={this.handleClickSearch}>
                                    WYSZUKAJ KURSY
                        </button>
                            </div>
                        </div>
                        {this.state.showActualRates ?
                            <CurrenciesList /> :
                            <LastRates />}
                    </div>
                </div>
            </div>

        );
    }
}

export default Main;
