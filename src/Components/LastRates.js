import React, { Component } from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from './Form'
import GetData from '../Data/GetData';

class LastRates extends React.Component {

    componentDidMount() {
        GetData.getData(`/tables/C/last?format=json`)
            .then(response =>
                this.setState({ codes: response.data[0].rates })
            )
    }

    handleClick = (event) => {
        event.preventDefault();
        GetData.getData(`/rates/C/${this.state.code}/last/${this.state.count}`)
            .then(response => {
                this.setState({ rates: response.data.rates });
            });
    };

    state = {
        code: 'USD',
        count: '10',
        codes: [],
        rates: []
    }

    render() {
        return (
            <div>
                <div className="row justify-content-center">
                    <form onSubmit={this.handleClick}>
                        <div className="input-group-sm mb-3 row">
                            <select className="custom-select"
                                onChange={(event) => this.setState({ code: event.target.value })}>
                                {this.state.codes.map((codes, i) =>
                                    <option key={i}
                                        className=""
                                        value={this.state.codes[i].code}>
                                        {this.state.codes[i].code}
                                    </option>
                                )}
                            </select>
    
                            <input type="number" className="form-control numberInput"
                                value={this.state.count}
                                onChange={(event) => this.setState({ count: event.target.value })}
                                placeholder=" COUNT" required />
                            <button className="btn btn-outline-success btn-search" type="submit" onClick={this.handleClick}>SEARCH</button>
                        </div>
                    </form>
                </div>
                <div className="row justify-content-center currency">
                    <div className="row col-12">
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">{this.state.code}</th>
                                    <th scope="col">Effective Date</th>
                                    <th scope="col">Bid</th>
                                    <th scope="col">Ask</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.rates.map((rate, i) =>
                                    <tr key={i}>
                                        <th scope="row">{i + 1}</th>
                                        <td>{this.state.rates[i].no}</td>
                                        <td>{this.state.rates[i].effectiveDate}</td>
                                        <td>{this.state.rates[i].bid}</td>
                                        <td>{this.state.rates[i].ask}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        )
    }
}

export default LastRates;