import React, { Component } from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import GetData from '../Data/GetData';

class DropDown extends React.Component {

    componentDidMount() {
        GetData.getData(`/tables/C/last?format=json`)
            .then(response =>
                this.setState({ rates: response.data[0].rates })
            )
    }

    state = {
        rates: []
    }

    render() {
        return (
            <select>
                {this.state.rates.map((rates, i) =>
                    <option key={i} value={this.state.rates[i].code}>{this.state.rates[i].code}</option>
                )}
            </select>
        )
    }
}

export default DropDown;