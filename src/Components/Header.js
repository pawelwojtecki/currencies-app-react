import React, { Component } from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import Clock from './Clock'
import Form from './Form'
import CurrenciesList from './CurrenciesList';
import LastRates from './LastRates'

class Header extends React.Component {

  render() {
    return (
      <nav className="navbar navbar-expand navbar-light bg-trasparent">
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav navbar-collapse mr-auto">
            <li className="nav-item active clock">
              <Clock />
            </li>
          </ul>
        </div>
        <div>
        </div>
      </nav>
    )
  }
}

export default Header;