import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date().toLocaleDateString(),
            time: new Date().toLocaleTimeString("ja-JP")
        }
    }

    componentDidMount() {
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillMount() {
        clearInterval(this.intervalID);
    }

    tick() {
        this.setState({
            date: new Date().toLocaleDateString(),
            time: new Date().toLocaleTimeString("ja-JP")
        });
    }

    render() {
        return (
            <div>
                {this.state.date + " " + this.state.time}
            </div>
        )
    }
}

export default Clock;