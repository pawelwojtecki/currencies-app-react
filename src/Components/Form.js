import React, { Component } from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import GetData from '../Data/GetData';
import DropDown from './DropDown'

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    state = {
        code: '',
        count: '',
        rates: []
    }

    handleClick = (event) => {
        event.preventDefault();
        GetData.getData(`/rates/C/${this.state.code}/last/${this.state.count}`)
            .then(response => {
                this.setState({ rates: response.data.rates });
                console.log(response.data.rates)
                console.log(this.state.rates);
            });
        console.log(this.state.code);
        console.log(this.state.count);
        console.log(this.state.rates);
    };

    render() {
        return (
            <form onSubmit={this.handleClick}>
                <div className="input-group-sm mb-3">
                    <DropDown 
                        value={this.state}
                        onClick={console.log(this.state.value)}/>
                    <input type="number" className="numberInput"
                        value={this.state.count}
                        onChange={(event) => this.setState({ count: event.target.value })}
                        placeholder=" COUNT" required />
                    <button type="submit" onClick={this.handleClick}>SEARCH</button>
                </div>
            </form>
        );
    }
}

export default Form;







// class Form extends React.Component {

//     state = { userName: ''}

//     handleSubmit = (event) => {
//         event.preventDefault();
//       axios.get(`https://api.github.com/users/${this.state.userName}`)
//           .then(resp => {
//             this.props.onSubmit(resp.data);
//         });
//     };

//       render() {
//         return (
//           <form onSubmit={this.handleSubmit}>
//             <input type="text" 
//                           value={this.state.userName}
//                   onChange={(event) => this.setState({ userName: event.target.value })}
//                     placeholder="Github username" required />
//           <button type="submit">Add card</button>
//         </form>
//       );
//     }

//   }

